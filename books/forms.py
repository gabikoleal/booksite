from django.forms import ModelForm
from .models import Book, Review, Provider


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'release_year',
            'poster_url',
        ]
        labels = {
            'name': 'Título',
            'release_year': 'Data de Lançamento',
            'poster_url': 'URL da Capa',
        }


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Resenha',
        }


class ProviderForm(ModelForm):
    class Meta:
        model = Provider
        fields = [
            'store',
            'has_stock',
            'price',
        ]
        labels = {
            'store': 'Onde Comprar',
            'has_stock': 'Fora de Estoque?',
            'price': 'Preço',
        }
