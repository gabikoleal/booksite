book_data = [{
    "id":
    "1",
    "name":
    "The Shawshank Redemption",
    "release_year":
    "1994",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg"
}, {
    "id":
    "2",
    "name":
    "The Godfather",
    "release_year":
    "1972",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/3bhkrj58Vtu7enYsRolD1fZdja1.jpg"
}, {
    "id":
    "3",
    "name":
    "Schindler's List",
    "release_year":
    "1993",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/sF1U4EUQS8YHUYjNl3pMGNIQyr0.jpg"
}, {
    "id":
    "4",
    "name":
    "Black Beauty",
    "release_year":
    "2020",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/5ZjMNJJabwBEnGVQoR2yoMEL9um.jpg"
}, {
    "id":
    "5",
    "name":
    "The Godfather: Part II",
    "release_year":
    "1974",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/hek3koDUyRQk7FIhPXsa6mT2Zc3.jpg"
}, {
    "id":
    "6",
    "name":
    "Your Name.",
    "release_year":
    "2016",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/q719jXXEzOoYaps6babgKnONONX.jpg"
}, {
    "id":
    "7",
    "name":
    "Spirited Away",
    "release_year":
    "2001",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/39wmItIWsg5sZMyRUHLkWBcuVCM.jpg"
}, {
    "id":
    "8",
    "name":
    "Hope",
    "release_year":
    "2013",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/x9yjkm9gIz5qI5fJMUTfBnWiB2o.jpg"
}, {
    "id":
    "9",
    "name":
    "Parasite",
    "release_year":
    "2019",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/7IiTTgloJzvGI1TAYymCfbfl3vT.jpg"
}, {
    "id":
    "10",
    "name":
    "My Hero Academia: Heroes Rising",
    "release_year":
    "2019",
    "cover_url":
    "https://image.tmdb.org/t/p/w500/zGVbrulkupqpbwgiNedkJPyQum4.jpg"
}
]